<?php

/**
 * Class SettingsTest
 */
class SettingsTest extends \Codeception\TestCase\WPTestCase {
	/**
	 * @var \SettingsMock
	 */
	private $settings;
	/**
	 * @var \Test_Plugin
	 */
	private $plugin;

	/**
	 *
	 * @throws \ComposePress\Core\Exception\Plugin
	 */
	public function setUp() {
		// before
		parent::setUp();
		$this->plugin   = test_plugin();
		$this->settings = $this->plugin->create_component( '\SettingsMock' );
	}

	/**
	 *
	 */
	public function tearDown() {
		// your tear down methods here

		// then
		parent::tearDown();
	}

	/**
	 *
	 */
	public function test_dotify() {
		$this->assertEquals( [ 'a.b.c' => 'd' ], $this->settings->dotify( [ 'a' => [ 'b' => [ 'c' => 'd' ] ] ] ) );
	}

	/**
	 *
	 */
	public function test_undotify() {
		$this->assertEquals( [ 'a' => [ 'b' => [ 'c' => 'd' ] ] ], $this->settings->undotify( [ 'a.b.c' => 'd' ] ) );
	}

	/**
	 *
	 */
	public function test_get_empty_option() {
		$this->assertFalse( $this->settings->get( 'test' ) );
	}

	/**
	 *
	 */
	public function test_get_dotify() {
		update_option( $this->plugin->safe_slug, [ 'test' => [ 'a' => 1 ] ] );

		$this->assertEquals( 1, $this->settings->get( 'test.a' ) );
	}

	public function test_get_default() {
		update_option( $this->plugin->safe_slug, [ 'test' => [ 'a' => 1 ] ] );
		$this->assertEquals( 'hello', $this->settings->get( 'test.b', 'hello' ) );
	}

	/**
	 *
	 */
	public function test_get_no_dotify() {
		update_option( $this->plugin->safe_slug, [ 'test' => [ 'a' => 1 ] ] );

		$this->assertEquals( [ 'a' => 1 ], $this->settings->get( 'test', null, false ) );
	}

	/**
	 *
	 */
	public function test_setting_name() {
		$this->assertEquals( 'test_plugin', $this->settings->get_setting_name() );
	}

	/**
	 *
	 */
	public function test_set() {
		$this->assertTrue( $this->settings->set( 'test.a', 1 ) );
		$this->assertEquals( 1, $this->settings->get( 'test.a' ) );
	}

	/**
	 *
	 */
	public function test_mass_set() {
		$this->assertTrue( $this->settings->batch_set( [ 'test' => [ 'a' => 1, 'b' => 2 ] ] ) );
		$this->assertEquals( 1, $this->settings->get( 'test.a' ) );
	}

	/**
	 *
	 */
	public function test_mass_get() {
		$this->assertTrue( $this->settings->batch_set( [ 'test' => [ 'a' => 1, 'b' => 2 ] ] ) );
		$this->assertEquals( [ 'test.a' => 1, 'test.b' => 2 ], $this->settings->batch_get( [ 'test.a', 'test.b' ] ) );
	}

	/**
	 *
	 */
	public function test_mass_get_empty() {
		$this->assertFalse( $this->settings->batch_get( [ 'test.a', 'test.b' ] ) );
	}
}
