<?php

namespace ComposePress\Settings\Abstracts;

use ComposePress\Core\Abstracts\Component_0_7_5_0;

/**
 * Class Settings
 */
abstract class Settings extends Component_0_7_5_0 {
	/**
	 * @var bool
	 */
	private $skip_dotify = false;

	/**
	 * @param      $option
	 * @param      $setting
	 * @param bool $dotify
	 *
	 * @return bool|mixed
	 */
	public function get( $setting, $default = null, $dotify = true ) {
		$option = get_option( static::get_setting_name() );
		if ( empty( $option ) ) {
			if ( null !== $default ) {
				return $default;
			}

			return false;
		}
		if ( $dotify ) {
			$option = $this->dotify( $option );
		}

		if ( isset( $option[ $setting ] ) ) {
			return $option[ $setting ];
		}

		if ( null !== $default ) {
			return $default;
		}

		return false;

	}

	/**
	 * @return string
	 */
	public function get_setting_name() {
		return $this->plugin->safe_slug;
	}

	/**
	 * @param        $options
	 * @param string $parent
	 *
	 * @return array
	 */
	public function dotify( $options, $parent = '' ) {
		if ( $this->skip_dotify ) {
			return $options;
		}

		$result = array();

		foreach ( $options as $key => $value ) {
			$new_key = $parent . ( empty( $parent ) ? '' : '.' ) . $key;

			if ( is_array( $value ) && 0 < count( array_filter( array_keys( $value ), 'is_string' ) ) ) {
				/** @noinspection PhpMethodParametersCountMismatchInspection */
				$result += $this->dotify( $value, $new_key );
			} else {
				$result[ $new_key ] = $value;
			}
		}

		return $result;
	}

	/**
	 * @param $option_name
	 * @param $settings
	 *
	 * @return mixed
	 */
	public function batch_set( array $settings ) {
		$option_name = static::get_setting_name();
		$option      = get_option( $option_name );

		if ( ! is_array( $option ) ) {
			$option = [];
		}

		$new_options = $this->undotify( $settings );

		$new_options = array_merge_recursive( $option, $new_options );

		$this->skip_dotify = true;

		$result            = update_option( $option_name, $new_options );
		$this->skip_dotify = false;

		return $result;
	}

	public function batch_get( array $settings, $defaults = null ) {
		$results = [];
		$option  = get_option( static::get_setting_name() );
		$empty   = empty( $option );
		if ( ! $empty ) {
			$option = $this->dotify( $option );
		}

		if ( $empty && null === $defaults ) {
			return false;
		}

		foreach ( $settings as $index => $setting ) {
			if ( is_array( $option ) && isset( $option[ $setting ] ) ) {
				$results[ $setting ] = $option[ $setting ];
				continue;
			}

			if ( $empty ) {
				if ( isset( $defaults[ $index ] ) ) {
					$results[ $setting ] = $defaults[ $index ];
					continue;
				}

				$results[ $setting ] = null;
			}
		}

		return $results;
	}

	/**
	 * @param $options
	 *
	 * @return array
	 */
	public function undotify( $options ) {
		$new_options = [];
		foreach ( (array) $options as $option => $value ) {
			$parts = explode( '.', $option );
			$count = substr_count( $option, '.' ) + 1;
			$last  = &$new_options;
			for ( $i = 0; $i < $count; $i ++ ) {
				if ( ! isset( $last[ $parts[ $i ] ] ) ) {
					$last[ $parts[ $i ] ] = [];
				}
				$last = &$last[ $parts[ $i ] ];

				if ( $i + 1 === $count ) {
					$last = $value;
				}
			}
		}

		return $new_options;
	}

	/**
	 * @param $option_name
	 * @param $setting
	 * @param $value
	 *
	 * @return mixed
	 */
	public function set( $setting, $value ) {
		$option_name = static::get_setting_name();
		$option      = get_option( $option_name );

		if ( ! is_array( $option ) ) {
			$option = [];
		}

		$new_options = [ $setting => $value ];
		$new_options = $this->undotify( $new_options );
		$new_options = array_merge_recursive( $option, $new_options );

		$this->skip_dotify = true;

		$result            = update_option( $option_name, $new_options );
		$this->skip_dotify = false;

		return $result;
	}
}
